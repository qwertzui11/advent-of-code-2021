use std::collections::HashSet;

fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input, 100));
    println!("part2: {}", part2(&input));
}

fn part2(numbers: &[i32]) -> usize {
    let mut numbers: Vec<i32> = numbers.iter().copied().collect();
    let mut counter = 0;
    while !numbers.windows(2).all(|a| a[0] == a[1]) {
        numbers = iterate_map(&numbers);
        counter += 1;
    }
    counter
}

fn part1(numbers: &[i32], iteration_count: i32) -> usize {
    let mut numbers: Vec<i32> = numbers.iter().copied().collect();
    let mut flash_count = 0;
    for _ in 0..iteration_count {
        numbers = iterate_map(&numbers);
        flash_count += numbers.iter().filter(|&a| *a == 0).count();
    }
    flash_count
}

fn iterate_map(numbers: &[i32]) -> Vec<i32> {
    let mut numbers: Vec<i32> = numbers.iter().copied().map(|a| a + 1).collect();
    let mut flashed = HashSet::new();
    let mut to_check: HashSet<usize> = numbers
        .iter()
        .enumerate()
        .filter(|a| *a.1 > 9)
        .map(|a| a.0)
        .collect();
    while !to_check.is_empty() {
        let current_index = *to_check.iter().next().unwrap();
        to_check.remove(&current_index);
        if numbers[current_index] <= 9 {
            continue;
        }
        assert!(!flashed.contains(&current_index));
        flashed.insert(current_index);
        let neighbours = [
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ];
        let x = (current_index % 10) as i32;
        let y = (current_index / 10) as i32;
        for neighbour in neighbours {
            let neighbour_x = x + neighbour.0;
            let neighbour_y = y + neighbour.1;
            if !(0..10).contains(&neighbour_x) || !(0..10).contains(&neighbour_y) {
                continue;
            }
            let neighbour_index = (neighbour_x + neighbour_y * 10) as usize;
            if flashed.contains(&neighbour_index) {
                continue;
            }
            if let Some(to_raise) = numbers.get_mut(neighbour_index) {
                *to_raise += 1;
                to_check.insert(neighbour_index);
            }
        }
    }
    numbers
        .iter()
        .copied()
        .map(|a| {
            if a > 9 {
                return 0;
            }
            a
        })
        .collect()
}

#[allow(dead_code)]
fn print_board(numbers: &[i32]) {
    for index in 0..10 {
        println!(
            "{:?}",
            numbers
                .iter()
                .skip(index * 10)
                .take(10)
                .map(|a| a.to_string())
                .collect::<Vec<String>>()
        );
    }
}

#[test]
fn test_part1_2() {
    let input = read_input("test_input2.txt");
    assert_eq!(part1(&input, 2), 3);
}

#[test]
fn test_part1() {
    let input = read_input("test_input.txt");
    assert_eq!(part1(&input, 100), 1656);
}

#[test]
fn test_part1_3() {
    let input = read_input("test_input.txt");
    assert_eq!(part1(&input, 10), 204);
}

fn read_input(file: &str) -> Vec<i32> {
    std::fs::read_to_string(file)
        .unwrap()
        .lines()
        .map(|a| {
            a.chars()
                .map(|b| b.to_digit(10).unwrap() as i32)
                .collect::<Vec<i32>>()
        })
        .flatten()
        .collect()
}
