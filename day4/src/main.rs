fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = std::fs::read_to_string("input.txt")?;
    let mut lines = input.lines();
    let drawn_numbers: Vec<i32> = lines
        .next()
        .unwrap()
        .split(',')
        .map(|a| a.parse::<i32>().unwrap())
        .collect();
    let mut boards = parse_boards(&mut lines);
    assert_eq!(boards.first().unwrap().lines.len(), 5);
    assert_eq!(boards.first().unwrap().lines.first().unwrap().len(), 5);

    let result_part1 = part1(&drawn_numbers, &mut boards);
    println!("part1: {}", result_part1);

    let result_part2 = part2(&drawn_numbers, boards);
    println!("part2: {}", result_part2);

    Ok(())
}

fn part2(drawn_numbers: &[i32], mut boards: Vec<Board>) -> i32 {
    for number in drawn_numbers {
        for board in boards.iter_mut() {
            apply_drawn_number_to_board(*number, board);
        }
        if boards.len() == 1 {
            if is_winning_board(boards.first().unwrap()) {
                return final_score(*number, boards.first().unwrap());
            }
        } else {
            boards.retain(|board| !is_winning_board(board));
        }
    }
    unreachable!();
}

fn apply_drawn_number_to_board(drawn: i32, board: &mut Board) {
    for board_line in &mut board.lines {
        for board_number in board_line.iter_mut() {
            if board_number.value == drawn {
                board_number.drawn = true;
            }
        }
    }
}

fn final_score(last_drawn: i32, board: &Board) -> i32 {
    let sum_not_drawn = board
        .lines
        .iter()
        .map(|a| a.iter().filter(|b| !b.drawn).map(|c| c.value).sum::<i32>())
        .sum::<i32>();
    sum_not_drawn * last_drawn
}

fn part1(drawn_numbers: &[i32], boards: &mut Vec<Board>) -> i32 {
    for number in drawn_numbers {
        for board in boards.iter_mut() {
            apply_drawn_number_to_board(*number, board);
            if is_winning_board(board) {
                return final_score(*number, board);
            }
        }
    }
    unreachable!();
}

fn is_winning_board(board: &Board) -> bool {
    let any_line = board.lines.iter().any(|a| a.iter().all(|b| b.drawn));
    let any_column = board
        .transposed_lines()
        .iter()
        .any(|a| a.iter().all(|b| b.drawn));
    any_line || any_column
}

fn parse_boards(parse: &mut std::str::Lines) -> Vec<Board> {
    let mut boards = Vec::new();
    while let Some(empty_line) = parse.next() {
        assert!(empty_line.is_empty());
        boards.push(parse_board(parse));
    }
    boards
}

fn parse_board(parse: &mut std::str::Lines) -> Board {
    let lines = parse.take(5).map(parse_line).collect();
    Board { lines }
}

fn parse_line(parse: &str) -> Vec<Number> {
    parse
        .split_whitespace()
        .map(|a| a.parse::<i32>().unwrap())
        .map(|value| Number {
            value,
            drawn: false,
        })
        .collect()
}

struct Board {
    lines: Vec<Vec<Number>>,
}

impl Board {
    fn transposed_lines(&self) -> Vec<Vec<Number>> {
        // https://stackoverflow.com/questions/64498617/how-to-transpose-a-vector-of-vectors-in-rust
        (0..self.lines[0].len())
            .map(|index| {
                self.lines
                    .iter()
                    .map(|inner| inner[index].clone())
                    .collect::<Vec<Number>>()
            })
            .collect()
    }
}

#[derive(Clone)]
struct Number {
    value: i32,
    drawn: bool,
}
