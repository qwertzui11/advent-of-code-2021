use std::collections::HashSet;

use itertools::Itertools;

fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

fn part2(input: &[(Vec<String>, Vec<String>)]) -> i32 {
    input.iter().map(part2_line).sum()
}

fn part2_line(line: &(Vec<String>, Vec<String>)) -> i32 {
    let (one, four, seven) = find_one_four_seven(line);
    let output_value: Vec<i32> = line
        .1
        .iter()
        .map(|a| letters_to_numbers(a, &one, &four, &seven))
        .collect();
    assert_eq!(output_value.len(), 4);
    output_value
        .into_iter()
        .fold(0, |result, a| result * 10 + a)
}

fn find_one_four_seven(search: &(Vec<String>, Vec<String>)) -> (String, String, String) {
    let all: Vec<_> = search.0.iter().chain(search.1.iter()).collect();
    let one = search_for_letter_length(&all, 2);
    let four = search_for_letter_length(&all, 4);
    let seven = search_for_letter_length(&all, 3);
    (one, four, seven)
}

fn search_for_letter_length(search: &[&String], target_length: usize) -> String {
    search
        .iter()
        .find(|a| a.len() == target_length)
        .unwrap()
        .to_string()
}

fn letters_to_numbers(letters: &str, one: &str, four: &str, seven: &str) -> i32 {
    let one_matches = letter_match_count(letters, one);
    let four_matches = letter_match_count(letters, four);
    let _seven_matches = letter_match_count(letters, seven);
    match (letters.len(), one_matches, four_matches) {
        (2, _, _) => 1,
        (4, _, _) => 4,
        (3, _, _) => 7,
        (7, _, _) => 8,
        (5, 1, 2) => 2,
        (5, 2, 3) => 3,
        (5, 1, 3) => 5,
        (6, 2, 3) => 0,
        (6, 1, 3) => 6,
        (6, 2, 4) => 9,
        (_, _, _) => unreachable!(),
    }
}

fn letter_match_count(first: &str, second: &str) -> usize {
    let first: HashSet<char> = first.chars().collect();
    let second: HashSet<char> = second.chars().collect();
    first.intersection(&second).count()
}

fn part1(input: &[(Vec<String>, Vec<String>)]) -> usize {
    let outputs: Vec<Vec<String>> = input.iter().map(|a| a.1.clone()).collect();
    outputs
        .iter()
        .map(|b| {
            b.iter()
                .filter(|c| is_number_with_unique_segment_count(c))
                .count()
        })
        .sum()
}

fn is_number_with_unique_segment_count(check: &str) -> bool {
    let unique_segment_number_lens = [
        2, // number 1
        4, // number 4
        3, // number 7
        7, // number 8
    ];
    unique_segment_number_lens.contains(&check.len())
}

fn read_input(file: &str) -> Vec<(Vec<String>, Vec<String>)> {
    std::fs::read_to_string(file)
        .unwrap()
        .lines()
        .map(parse_line)
        .collect()
}

fn parse_line(line: &str) -> (Vec<String>, Vec<String>) {
    line.split('|')
        .map(parse_word_list)
        .collect_tuple()
        .unwrap()
}

fn parse_word_list(words: &str) -> Vec<String> {
    words.split_whitespace().map(|a| a.to_string()).collect()
}
