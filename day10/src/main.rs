fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

fn part2(lines: &[String]) -> i64 {
    let mut result: Vec<i64> = lines
        .iter()
        .map(|a| {
            let chars: Vec<_> = a.chars().collect();
            part2_line(&chars)
        })
        .filter(|&a| a > 0)
        .collect();
    assert_eq!(result.len() % 2, 1);
    result.sort_unstable();
    *result.get(result.len() / 2).unwrap()
}

#[test]
fn test_part2() {
    let input = read_input("test_input.txt");
    assert_eq!(part2(&input), 288957);
}

fn part2_line(mut check: &[char]) -> i64 {
    let mut stack = Vec::new();
    while !check.is_empty() {
        let current = *check.first().unwrap();
        if is_opening(current) {
            stack.push(closing_for_opening(current));
        } else if let Some(closing) = stack.pop() {
            if closing != current {
                return 0;
            }
        }
        check = &check[1..];
    }
    stack.iter().rev().fold(0, |result, a| {
        result * 5
            + match a {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                '>' => 4,
                _ => unreachable!(),
            }
    })
}

fn part1(lines: &[String]) -> i32 {
    lines
        .iter()
        .map(|a| {
            let chars: Vec<_> = a.chars().collect();
            part1_line(&chars)
        })
        .sum()
}

fn part1_line(mut check: &[char]) -> i32 {
    let mut stack = Vec::new();
    while !check.is_empty() {
        let current = *check.first().unwrap();
        if is_opening(current) {
            stack.push(closing_for_opening(current));
        } else if let Some(closing) = stack.pop() {
            if closing != current {
                return match current {
                    '}' => 1197,
                    ')' => 3,
                    '>' => 25137,
                    ']' => 57,
                    _ => unreachable!(),
                };
            }
        }
        check = &check[1..];
    }
    0
}

fn is_opening(check: char) -> bool {
    matches!(check, '{' | '<' | '(' | '[')
}

fn closing_for_opening(opening: char) -> char {
    match opening {
        '{' => '}',
        '[' => ']',
        '(' => ')',
        '<' => '>',
        _ => unreachable!(),
    }
}

fn read_input(file: &str) -> Vec<String> {
    std::fs::read_to_string(file)
        .unwrap()
        .lines()
        .map(|a| a.to_string())
        .collect()
}
