fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = std::fs::read_to_string("input.txt")?;
    let directions: Vec<Direction> = input.lines().map(parse_line).collect();
    {
        let mut x = 0;
        let mut depth = 0;
        for direction in &directions {
            match direction {
                Direction::Forward(distance) => x += distance,
                Direction::Down(distance) => depth += distance,
                Direction::Up(distance) => depth -= distance,
            }
            assert!(depth >= 0);
        }
        println!("part1: {}", x * depth);
    }
    {
        let mut x = 0;
        let mut depth = 0;
        let mut aim = 0;
        for direction in &directions {
            match direction {
                Direction::Forward(distance) => {
                    x += distance;
                    depth += aim * distance;
                }
                Direction::Down(distance) => aim += distance,
                Direction::Up(distance) => aim -= distance,
            }
            assert!(depth >= 0);
        }
        println!("part2: {}", x * depth);
    }
    Ok(())
}

fn parse_line(parse: &str) -> Direction {
    let mut words = parse.split_whitespace();
    let direction = words.next().unwrap();
    let distance = words.next().unwrap().parse::<i32>().unwrap();
    match direction {
        "forward" => Direction::Forward(distance),
        "down" => Direction::Down(distance),
        "up" => Direction::Up(distance),
        _ => unreachable!(),
    }
}

enum Direction {
    Forward(i32),
    Down(i32),
    Up(i32),
}
