fn main() {
    let input = parse_input("input.txt");
    println!("part1: {}", part1(input.clone(), 80));
    println!("part2: {}", part2(&input, 256));
    {
        let mut _numbers = input;
    }
}

fn part2(numbers: &[i32], days: i32) -> i128 {
    const GROUP_COUNT: usize = 9;
    let mut groups = [0i128; GROUP_COUNT];
    for number in numbers {
        groups[*number as usize] += 1;
    }
    for _day in 1..=days {
        let mut new_groups = [0; GROUP_COUNT];
        new_groups[..(GROUP_COUNT - 1)].clone_from_slice(&groups[1..GROUP_COUNT]);
        new_groups[8] = groups[0];
        new_groups[6] += groups[0];
        groups = new_groups;
    }
    groups.into_iter().sum()
}

fn part1(mut numbers: Vec<i32>, days: i32) -> usize {
    for _day in 1..=days {
        let aged = numbers.clone().into_iter().map(reduced_life);
        let children = numbers.into_iter().filter(|&a| a == 0).map(|_| 8);
        numbers = aged.into_iter().chain(children.into_iter()).collect();
    }
    numbers.len()
}

#[test]
fn test_part1() {
    let input = parse_input("test_input.txt");
    assert_eq!(part1(input.clone(), 2), 6);
    assert_eq!(part1(input.clone(), 3), 7);
    assert_ne!(part1(input.clone(), 18), 20);
    assert_eq!(part1(input, 80), 5934);
}

#[test]
fn test_part2() {
    let input = parse_input("test_input.txt");
    assert_eq!(part2(&input, 80), 5934);
    assert_eq!(part2(&input, 256), 26984457539);
}

fn reduced_life(mut input: i32) -> i32 {
    input -= 1;
    if input < 0 {
        return 6;
    }
    input
}

fn parse_input(file: &str) -> Vec<i32> {
    let input = std::fs::read_to_string(file).unwrap();
    input
        .trim()
        .split(',')
        .map(|a| a.parse::<i32>().unwrap())
        .collect()
}
