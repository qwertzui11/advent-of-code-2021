fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

#[test]
fn test_part2() {
    let input = read_input("test_input.txt");
    assert_eq!(part2(&input), 36);
}

#[test]
fn test_part2_2() {
    let input = read_input("test_input2.txt");
    assert_eq!(part2(&input), 103);
}

#[test]
fn test_part2_3() {
    let input = read_input("test_input3.txt");
    assert_eq!(part2(&input), 3509);
}

fn part2(segments: &[(String, String)]) -> i32 {
    let alrady_seen_small_caves = vec![];
    get_filtered_segments(segments, "start")
        .iter()
        .fold(0, |result, a| {
            result + recursive_part2(segments, alrady_seen_small_caves.clone(), a, false)
        })
}

fn recursive_part2(
    segments: &[(String, String)],
    mut alrady_seen_small_caves: Vec<String>,
    current: &str,
    mut one_twice: bool,
) -> i32 {
    if current == "end" {
        return 1;
    }
    if current == "start" {
        return 0;
    }
    if alrady_seen_small_caves.iter().any(|a| *a == current) {
        if !one_twice {
            one_twice = true;
        } else {
            return 0;
        }
    }
    if current.to_lowercase() == current {
        alrady_seen_small_caves.push(current.to_string());
    }
    get_filtered_segments(segments, current)
        .iter()
        .fold(0, |result, a| {
            result + recursive_part2(segments, alrady_seen_small_caves.clone(), a, one_twice)
        })
}

fn part1(segments: &[(String, String)]) -> i32 {
    let alrady_seen_small_caves = vec!["start".to_string()];
    get_filtered_segments(segments, "start")
        .iter()
        .fold(0, |result, a| {
            result + recursive_part1(segments, alrady_seen_small_caves.clone(), a.to_string())
        })
}

fn recursive_part1(
    segments: &[(String, String)],
    mut alrady_seen_small_caves: Vec<String>,
    current: String,
) -> i32 {
    if current == "end" {
        return 1;
    }
    if alrady_seen_small_caves.iter().any(|a| *a == current) {
        return 0;
    }
    if current.to_lowercase() == current {
        alrady_seen_small_caves.push(current.clone());
    }
    get_filtered_segments(segments, &current)
        .iter()
        .fold(0, |result, a| {
            result + recursive_part1(segments, alrady_seen_small_caves.clone(), a.to_string())
        })
}

fn get_filtered_segments(segments: &[(String, String)], search: &str) -> Vec<String> {
    segments
        .iter()
        .filter(|a| a.0 == search || a.1 == search)
        .map(|a| {
            if a.0 == search {
                a.1.clone()
            } else {
                a.0.clone()
            }
        })
        .collect()
}

fn read_input(file: &str) -> Vec<(String, String)> {
    std::fs::read_to_string(file)
        .unwrap()
        .lines()
        .map(parse_line)
        .collect()
}

fn parse_line(line: &str) -> (String, String) {
    let splitted: Vec<String> = line.split('-').map(|a| a.to_string()).collect();
    (splitted[0].clone(), splitted[1].clone())
}
