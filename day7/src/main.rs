fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

fn part2(numbers: &[i32]) -> i32 {
    let min_position = *numbers.iter().min().unwrap();
    let max_position = *numbers.iter().max().unwrap();
    (min_position..=max_position)
        .map(|target_position| {
            numbers
                .iter()
                .map(|&a| distance_triangle_number((target_position - a).abs()))
                .sum::<i32>()
        })
        .min()
        .unwrap()
}

fn distance_triangle_number(number: i32) -> i32 {
    // https://en.wikipedia.org/wiki/Triangular_number
    (number * number + number) / 2
}

fn part1(numbers: &[i32]) -> i32 {
    let min_position = *numbers.iter().min().unwrap();
    let max_position = *numbers.iter().max().unwrap();
    (min_position..=max_position)
        .map(|target_position| {
            numbers
                .iter()
                .map(|&a| (target_position - a).abs())
                .sum::<i32>()
        })
        .min()
        .unwrap()
}

fn read_input(file: &str) -> Vec<i32> {
    std::fs::read_to_string(file)
        .unwrap()
        .trim()
        .split(',')
        .map(|a| a.parse().unwrap())
        .collect()
}
