fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = std::fs::read_to_string("input.txt")?;
    let numbers: Vec<Vec<i32>> = input.split_whitespace().map(parse_line).collect();
    let bit_count = numbers.first().unwrap().len();
    {
        let mut gamma = 0;
        let mut epsilon = 0;
        for bit_index in 0..bit_count {
            let gamma_bit = numbers.iter().map(|a| a[bit_index]).sum::<i32>();
            let bit_as_number = 1 << (bit_count - (bit_index + 1));
            if gamma_bit > numbers.len() as i32 / 2 {
                gamma += bit_as_number;
            } else {
                epsilon += bit_as_number;
            }
        }
        println!(
            "part1: gamma: {}, epsilon: {}, energy: {}",
            gamma,
            epsilon,
            gamma * epsilon
        );
    }
    {
        let (oxygen, co2) = part2(numbers);
        println!(
            "part2, oxygen: {}, co2: {}, result: {}",
            oxygen,
            co2,
            oxygen * co2
        );
    }
    Ok(())
}

fn part2(numbers: Vec<Vec<i32>>) -> (i32, i32) {
    let bit_count = numbers.first().unwrap().len();
    let mut filtered_numbers = numbers.clone();
    for bit_index in 0..bit_count {
        let one_count = filtered_numbers.iter().map(|a| a[bit_index]).sum::<i32>();
        let more_one_than_zeros = one_count >= (filtered_numbers.len() as i32 - one_count);
        let bit_to_keep = more_one_than_zeros as i32;
        filtered_numbers = filtered_numbers
            .into_iter()
            .filter(|a| a[bit_index] == bit_to_keep)
            .collect();
        if filtered_numbers.len() == 1 {
            break;
        }
    }
    assert_eq!(filtered_numbers.len(), 1);
    let oxygen = bits_to_number(filtered_numbers.first().unwrap());

    let mut filtered_numbers = numbers;
    for bit_index in 0..bit_count {
        let one_count = filtered_numbers.iter().map(|a| a[bit_index]).sum::<i32>();
        let more_one_than_zeros = one_count >= (filtered_numbers.len() as i32 - one_count);
        let bit_to_keep = 1 - more_one_than_zeros as i32;
        filtered_numbers = filtered_numbers
            .into_iter()
            .filter(|a| a[bit_index] == bit_to_keep)
            .collect();
        if filtered_numbers.len() == 1 {
            break;
        }
    }
    assert_eq!(filtered_numbers.len(), 1);
    let co2 = bits_to_number(filtered_numbers.first().unwrap());
    (oxygen, co2)
}

#[test]
fn test_part2() {
    let input = std::fs::read_to_string("test_input.txt").unwrap();
    let numbers: Vec<Vec<i32>> = input.split_whitespace().map(parse_line).collect();
    assert_eq!(part2(numbers), (23, 10));
}

fn bits_to_number(bits: &[i32]) -> i32 {
    let mut result = 0;
    let bit_count = bits.len();
    for (index, bit) in bits.iter().enumerate() {
        result += bit << (bit_count - (index + 1));
    }
    result
}

#[test]
fn test_bits_to_number() {
    assert_eq!(bits_to_number(&[1, 1, 1]), 7);
    assert_eq!(bits_to_number(&[1, 0, 1]), 5);
    assert_eq!(bits_to_number(&[0, 0, 1]), 1);
    assert_eq!(bits_to_number(&[1, 0, 0]), 4);
    assert_eq!(bits_to_number(&[1, 1, 0, 0]), 12);
    assert_eq!(bits_to_number(&[1, 1, 0, 1]), 13);
}

fn parse_line(line: &str) -> Vec<i32> {
    line.chars()
        .map(|a| match a {
            '1' => 1,
            '0' => 0,
            _ => unreachable!(),
        })
        .collect()
}
