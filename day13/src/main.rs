use std::collections::HashSet;

use itertools::Itertools;

fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input.0, &input.1));
    part2(input.0, &input.1);
}

fn part2(mut input: HashSet<(i32, i32)>, folds: &[Fold]) {
    for fold in folds {
        input = fold_map(&input, fold);
    }
    print_map(&input);
}

fn print_map(input: &HashSet<(i32, i32)>) {
    let max_x = input.iter().max_by_key(|a| a.0).unwrap().0;
    let max_y = input.iter().max_by_key(|a| a.1).unwrap().1;
    for y in 0..=max_y {
        for x in 0..=max_x {
            if input.get(&(x, y)).is_some() {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
}

#[test]
fn test_part2() {
    let input = read_input("test_input.txt");
    part2(input.0, &input.1);
}

fn part1(input: &HashSet<(i32, i32)>, folds: &[Fold]) -> i32 {
    let fold = folds.first().unwrap();
    fold_map(input, fold).len() as i32
}

fn fold_map(map: &HashSet<(i32, i32)>, fold: &Fold) -> HashSet<(i32, i32)> {
    let mut result = HashSet::new();
    for item in map {
        result.insert(match fold {
            Fold::AlongX(x) => {
                if item.0 > *x {
                    ((x - 1) - (item.0 - (x + 1)), item.1)
                } else {
                    *item
                }
            }
            Fold::AlongY(y) => {
                if item.1 > *y {
                    (item.0, (y - 1) - (item.1 - (y + 1)))
                } else {
                    *item
                }
            }
        });
    }
    result
}

fn read_input(file: &str) -> (HashSet<(i32, i32)>, Vec<Fold>) {
    let file_content = std::fs::read_to_string(file).unwrap();
    let mut lines = file_content.lines();
    let coords: HashSet<(i32, i32)> = lines
        .by_ref()
        .take_while(|a| !a.is_empty())
        .map(|a| {
            a.split(',')
                .map(|b| b.parse::<i32>().unwrap())
                .collect_tuple::<(i32, i32)>()
                .unwrap()
        })
        .collect();
    let folds: Vec<Fold> = lines
        .map(|a| {
            parse_fold(
                a.split_whitespace()
                    .last()
                    .unwrap()
                    .split('=')
                    .collect_tuple::<(&str, &str)>()
                    .unwrap(),
            )
        })
        .collect();
    (coords, folds)
}

fn parse_fold(parse: (&str, &str)) -> Fold {
    let number = parse.1.parse::<i32>().unwrap();
    match parse.0 {
        "x" => Fold::AlongX(number),
        "y" => Fold::AlongY(number),
        _ => unreachable!(),
    }
}

enum Fold {
    AlongX(i32),
    AlongY(i32),
}
