use std::fs;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input = fs::read_to_string("input.txt")?;
    let numbers: Vec<i32> = input
        .split_whitespace()
        .map(|a| a.parse().unwrap())
        .collect();
    {
        let part1 = numbers
            .windows(2)
            .map(|a| a[1] - a[0])
            .filter(|&a| a > 0)
            .count();
        println!("part1: {}", part1);
    }
    {
        let window_sums: Vec<i32> = numbers.windows(3).map(|a| a.iter().sum()).collect();
        let part2 = window_sums
            .windows(2)
            .map(|a| a[1] - a[0])
            .filter(|&a| a > 0)
            .count();
        println!("part2: {}", part2);
    }
    Ok(())
}
