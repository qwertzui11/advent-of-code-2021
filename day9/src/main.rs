use std::collections::HashSet;

fn main() {
    let input = read_input("input.txt");
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

fn part2(heightmap: &[Vec<i32>]) -> i32 {
    let mut basins: Vec<_> = heightmap
        .iter()
        .enumerate()
        .map(|(y, lines)| {
            lines
                .iter()
                .enumerate()
                .map(|(x, &check)| {
                    (
                        is_low_point(heightmap, x as i32, y as i32, check),
                        x as i32,
                        y as i32,
                    )
                })
                .filter(|a| a.0)
                .map(|b| basin_size(heightmap, b.1, b.2))
                .collect::<Vec<_>>()
        })
        .flatten()
        .collect();
    basins.sort_unstable();
    basins.iter().rev().take(3).product()
}

#[test]
fn test_part2() {
    let input = read_input("test_input.txt");
    assert_eq!(part2(&input), 1134);
}

fn basin_size(heightmap: &[Vec<i32>], x: i32, y: i32) -> i32 {
    let mut cache = HashSet::<(i32, i32)>::new();
    cache.insert((x, y));
    loop {
        let mut add: HashSet<(i32, i32)> = HashSet::new();
        for check in &cache {
            add.insert(*check);
            let neighbours_smaller_nine = get_neighbours(heightmap, check.0, check.1)
                .into_iter()
                .filter(|a| a.2 < 9)
                .map(|b| (b.0, b.1))
                .collect::<HashSet<_>>();
            add.extend(neighbours_smaller_nine.into_iter());
        }
        if add.len() == cache.len() {
            return cache.len() as i32;
        }
        cache = add;
    }
}

fn get_neighbours(heightmap: &[Vec<i32>], x: i32, y: i32) -> Vec<(i32, i32, i32)> {
    let checks: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];
    let mut result = Vec::new();
    for check in checks {
        let x = x + check.0;
        let y = y + check.1;
        result.push((
            x,
            y,
            get_number_in_heightmap(heightmap, x as usize, y as usize).unwrap_or(9),
        ));
    }
    result
}

fn part1(heightmap: &[Vec<i32>]) -> i32 {
    heightmap
        .iter()
        .enumerate()
        .map(|(y, lines)| {
            lines
                .iter()
                .enumerate()
                .map(|(x, &check)| {
                    (
                        is_low_point(heightmap, x as i32, y as i32, check),
                        check + 1,
                    )
                })
                .filter(|a| a.0)
                .map(|b| b.1)
                .sum::<i32>()
        })
        .sum()
}

fn is_low_point(heightmap: &[Vec<i32>], x: i32, y: i32, number: i32) -> bool {
    let checks: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];
    for check in checks {
        if number
            >= get_number_in_heightmap(heightmap, (x + check.0) as usize, (y + check.1) as usize)
                .unwrap_or(10)
        {
            return false;
        }
    }
    true
}

fn get_number_in_heightmap(heightmap: &[Vec<i32>], x: usize, y: usize) -> Option<i32> {
    if let Some(line) = heightmap.get(y) {
        if let Some(&line_number) = line.get(x) {
            return Some(line_number);
        }
    }
    None
}

fn read_input(file: &str) -> Vec<Vec<i32>> {
    std::fs::read_to_string(file)
        .unwrap()
        .lines()
        .map(|a| a.chars().map(|b| b.to_digit(10).unwrap() as i32).collect())
        .collect()
}
