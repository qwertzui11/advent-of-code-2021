use std::collections::HashMap;

fn main() {
    let coords = parse_input("input.txt");
    println!("part1: {}", part1(&coords));
    println!("part2: {}", part2(&coords));
}

fn part2(input: &[Coords]) -> i32 {
    let mut covered: HashMap<Coord, i32> = HashMap::new();
    for segment in input {
        let mut current = segment.from;
        add_one_to_map(&mut covered, current);
        while current != segment.to {
            current.x += segment.dir_x();
            current.y += segment.dir_y();
            add_one_to_map(&mut covered, current);
        }
    }
    covered.into_values().filter(|&a| a > 1).count() as i32
}

#[test]
fn test_part2() {
    let input = parse_input("test_input.txt");
    assert_eq!(part2(&input), 12);
}

fn part1(input: &[Coords]) -> i32 {
    let mut covered: HashMap<Coord, i32> = HashMap::new();
    for segment in input.iter().filter(|a| is_horizontal_or_vertical(a)) {
        let mut current = segment.from;
        add_one_to_map(&mut covered, current);
        while current != segment.to {
            current.x += segment.dir_x();
            current.y += segment.dir_y();
            add_one_to_map(&mut covered, current);
        }
    }
    covered.into_values().filter(|&a| a > 1).count() as i32
}

fn add_one_to_map(map: &mut HashMap<Coord, i32>, coord: Coord) {
    if let Some(raise) = map.get_mut(&coord) {
        *raise += 1;
    } else {
        map.insert(coord, 1);
    }
}

#[test]
fn test_part1() {
    let input = parse_input("test_input.txt");
    assert_eq!(part1(&input), 5);
}

fn parse_input(file: &str) -> Vec<Coords> {
    let input = std::fs::read_to_string(file).unwrap();
    input.lines().map(parse_line).collect()
}

fn parse_line(line: &str) -> Coords {
    let numbers: Vec<Coord> = line.split("->").map(parse_coord).collect();
    Coords::new(numbers[0], numbers[1])
}

fn parse_coord(parse: &str) -> Coord {
    let numbers = parse
        .trim()
        .split(',')
        .map(|b| b.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();
    Coord {
        x: numbers[0],
        y: numbers[1],
    }
}

#[derive(Clone, Debug)]
struct Coords {
    from: Coord,
    to: Coord,
}

impl Coords {
    fn new(from: Coord, to: Coord) -> Self {
        Coords { from, to }
    }

    // return (-1, 0, 1) depending on the direction
    fn dir_x(&self) -> i32 {
        let dir = self.to.x - self.from.x;
        if dir == 0 {
            return 0;
        }
        let length = dir.abs();
        dir / length
    }
    fn dir_y(&self) -> i32 {
        let dir = self.to.y - self.from.y;
        if dir == 0 {
            return 0;
        }
        let length = dir.abs();
        dir / length
    }
}

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
struct Coord {
    x: i32,
    y: i32,
}

fn is_horizontal_or_vertical(coord: &Coords) -> bool {
    coord.from.x == coord.to.x || coord.from.y == coord.to.y
}

#[test]
fn test_is_horizontal_or_vertical() {
    assert!(!is_horizontal_or_vertical(&Coords {
        from: Coord { x: 8, y: 0 },
        to: Coord { x: 0, y: 8 }
    }))
}
